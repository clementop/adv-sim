//
// adv-sim

function newGame() {
  game = new Game();
  game.addAdventurer(new Adventurer({gold: 100, level: 1}));

  var $inn = $(".inn .name")[0];
  $inn.innerHTML = "Inn: " + game.inn;
  var $adventurers = $(".inn .adventurers")[0];
  $adventurers.innerHTML = "";
  game.adventurers.forEach(function (adventurer) {
    $adventurers.appendChild(adventurer.toHtml());
  });

  return game;
}

function Game(options) {
  options = options || {}
  this.inn = options.inn || "The " + random(DATA.INN.ADJECTIVES) + " " + random(DATA.INN.NOUNS);
  this.adventurers = [];

  this.addAdventurer = function(adventurer) {
    this.adventurers.push(adventurer);
  }

  this._sequences = {
    adv: new Sequence('adv', options.seq_ad)
  }

  this.nextId = function(sequence) {
    return this._sequences[sequence].next();
  }
}

function Sequence(prefix, starting) {
  this.prefix = prefix;
  this.value = starting || 0;

  this.next = function() {
    return '' + prefix + ++this.value;
  }
}

function Adventurer(options) {
  options = options || {};

  this.id = game.nextId("adv");

  this.name = options.name || random(random([DATA.NAMES_MALE, DATA.NAMES_FEMALE]));
  this.nickname = options.nickname || chance(25) ? random(DATA.NICKNAMES) : undefined;

  this.level = options.level || 0; // 0 is a retainer
  this.gold = options.gold || 0;

  this.race = options.race || random(DATA.RACES);
  this.prof = options.prof || random(DATA.CLASSES);

  this.getName = function () {
    return this.nickname ? this.name + ' ' + this.nickname : this.name;
  }

  this.toHtml = function () {
    var content = " - " + this.getName() + " ("
      + this.race + " " + this.prof
      + ", level " + this.level
      + ", " + this.gold + "g)";
    return $el("div", this.id, "adventurer", content);
  }

  return this;
}

function random(list) {
  return list[Math.floor(Math.random() * list.length)];
}

function chance(percentage) {
  return Math.floor(Math.random() * 100) <= percentage;
}

function $(query) {
  return document.querySelectorAll(query);
}

function $id(id){
  return document.getElementById(id);
}

function $el(tag, id, className, html) {
  var element = document.createElement(tag);
  element.id = id;
  element.className = className;
  element.innerHTML = html;
  return element;
}

DATA = {
  INN: {
    ADJECTIVES: ['Accidental', 'Award-Winning', 'Boiled', 'Bored', 'Busy', 'Clumsy', 'Cold', 'Cramped', 'Creaky', 'Damp', 'Derelict', 'Dusty', 'Fat', 'Flimsy', 'Forgotten', 'Fortunate', 'Frantic', 'Greasy', 'Leaky', 'Lonely', 'Noisy', 'Quiet', 'Stubborn', 'Stuck', 'Unfortunate', 'Wealthy'],
    NOUNS: ['Bar', 'Bear', 'Bird', 'Broom', 'Farm', 'Fireplace', 'Fork', 'Fountain', 'Kettle', 'Mead', 'Mug', 'Pig', 'Pilgrim', 'Scepter', 'Shield', 'Skald', 'Soup', 'Spoon', 'Squirrel', 'Sword', 'Wolf'],
  },
  CLASSES: ['Fighter', 'Bard', 'Druid', 'Wizard', 'Ranger', 'Berserker', 'Cleric'],
  RACES: ['Human', 'Elf', 'Dwarf', 'Halfling'],
  NAMES_MALE: ['Aella', 'Agdi', 'Agnar', 'Alrek', 'An', 'Angantyr', 'Aran', 'Armod', 'Arnfinn', 'Arngrim', 'Asmund', 'Atli', 'Auda', 'Bard', 'Barri', 'Beiti', 'Bild', 'Bjarkmar', 'Borgar', 'Bosi', 'Brand', 'Brynjolf', 'Budli', 'Bui', 'Drott', 'Eddval', 'Egli', 'Einar', 'Eirik', 'Eitil', 'Erp', 'Eylimi', 'Eyolf', 'Eystein', 'Fafnir', 'Finnbogi', 'Fjolmod', 'Fjolvar', 'Fjori', 'Franmar', 'Freki', 'Fridleif', 'Frithjof', 'Frodi', 'Frosti', 'Fyri', 'Gardar', 'Gauk', 'Gauti', 'Gautrek', 'Geirmund', 'Geirrod', 'Geirthjof', 'Gilling', 'Gjuki', 'Glammad', 'Gothorm', 'Granmar', 'Grettir', 'Grim', 'Grimhild', 'Gripir', 'Grundi', 'Gudmund', 'Gunnar', 'Gunnbjorn', 'Gust', 'Guthorm', 'Hadding', 'Haeming', 'Hafgrim', 'Hagal', 'Hak', 'Haki', 'Hakon', 'Halfdan', 'Hamal', 'Hamdir', 'Harald', 'Harek', 'Hauk', 'Havard', 'Hedin', 'Heidrek', 'Heimir', 'Helgi', 'Herbjorn', 'Herthjof', 'Hervard', 'Hildigrim', 'Hjalmar', 'Hjalprek', 'Hjorleif', 'Hjorolf', 'Hjorvard', 'Hlodvard', 'Hlodver', 'Hlothver', 'Hodbrodd', 'Hogni', 'Holgeir', 'Hosvir', 'Hraerek', 'Hrafknel', 'Hrani', 'Hreggvid', 'Hring', 'Hroar', 'Hrodmar', 'Hroi', 'Hrolf', 'Hrollaug', 'Hrosskel', 'Hrotti', 'Hunding', 'Hunthjof', 'Hymling', 'Idmund', 'Illugi', 'Imsigull', 'Ingjald', 'Ivar', 'Jarnskeggi', 'Jokul', 'Jormunrek', 'Ketil', 'Kjar', 'Knui', 'Kol', 'Krabbi', 'Kraki', 'Leif', 'Melnir', 'Neri', 'Odd', 'Olaf', 'Olvir', 'Orkning', 'Orr', 'Otrygg', 'Ottar', 'Raevil', 'Raknar', 'Ref', 'Rennir', 'Rodstaff', 'Rolf', 'Runolf', 'Saedmund', 'Sigmund', 'Sigurd', 'Sinfjotli', 'Sirnir', 'Sjolf', 'Skuli', 'Skuma', 'Slagfid', 'Smid', 'Snaevar', 'Snidil', 'Snorri', 'Sorkvir', 'Sorli', 'Soti', 'Starkad', 'Steinthor', 'Storvirk', 'Styr', 'Svafnir', 'Svafrlami', 'Svart', 'Svidi', 'Svip', 'Thjodrek', 'Thord', 'Thorfinn', 'Thorgeir', 'Thorir', 'Thormod', 'Thorstein', 'Thrand', 'Thvari', 'Tind', 'Toki', 'Tryfing', 'Ulf', 'Ulfhedin', 'Vidgrip', 'Vignir', 'Vikar', 'Visin', 'Volund'],
  NAMES_FEMALE: ['Adis', 'Aesa', 'Afrid', 'Aggi', 'Alfhild', 'Alof', 'Arnora', 'Asa', 'Aslaug', 'Astrid', 'Aud', 'Baza', 'Bekkhild', 'Bera', 'Bergdis', 'Bestla', 'Bodvild', 'Borghild', 'Borgny', 'Brynhild', 'Busla', 'Dagmaer', 'Dagny', 'Dyrhild', 'Edda', 'Edny', 'Erva', 'Eyfura', 'Fjotra', 'Freydis', 'Galumvor', 'Geirrid', 'Gjaflaug', 'Grimhild', 'Groa', 'Gudrid', 'Gudrun', 'Gullrond', 'Halla', 'Halldis', 'Hallfrid', 'Hallveig', 'Helga', 'Herborg', 'Herkja', 'Hervor', 'Hildigunn', 'Hildirid', 'Hjordis', 'Hjotra', 'Hleid', 'Hrafnhild', 'Hrodrglod', 'Ingibjorg', 'Isgerd', 'Jora', 'Kara', 'Kolfrosta', 'Kostbera', 'Laila', 'Linna', 'Lofnheid', 'Lofthaena', 'Lyngheid', 'Nauma', 'Oddrun', 'Olvor', 'Ragnhild', 'Romilda', 'Runa', 'Saereid', 'Sida', 'Sigrid', 'Sigrlinn', 'Sigrun', 'Silksif', 'Sinrjod', 'Skjalf', 'Svana', 'Svanhvit', 'Swanhild', 'Sylgja', 'Thjodhild', 'Thorgerd', 'Thorunn', 'Throa', 'Thurid', 'Tofa', 'Unn', 'Vaetild', 'Vedis', 'Yrsa'],
  NICKNAMES: ['the Absent-Minded', 'Ale-Lover', 'Arrow-Odd', 'Bag-Nose', 'Bare-Legs', 'the Belly-Shaker', 'the Berserk-Killer', 'the Big', 'the Black', 'the Blind', 'Blood-Axe', 'the Bold', 'the Brave', 'Bull-Bear', 'Buttered-Bread', 'the Child-Sparer', 'the Clumsy', 'Coal-Brow', 'the Crow', 'the Deep-Minded', 'the Drunk', 'the Dueller', 'the Easterner', 'the Fair', 'the Feeble', 'the Fetter-Hound', 'Finehair', 'Fish-Hook', 'Flat-Nose', 'the Flayer', 'the Fosterer', 'the Frantic', 'the Good', 'the Gossip', 'Grey-Cloak', 'Hairy-Breeks', 'Hairy-Cheek', 'Half-Hand', 'Hard-Mouth', 'the Horse-Gelder', 'Hot-Head', 'the Hunter', 'the Keen-Eyed', 'the Lean', 'Leather-Neck', 'Limp-Leg', 'Little', 'Long-Leg', 'Long-Pants', 'the Lucky', 'Mansion-Might', 'Moss-Neck', 'Night-Sun', 'the Noisy', 'the Old', 'One-Hand', 'the Peaceful', 'the Peacock', 'the Pilgrim', 'Pin-Leg', 'the Powerful', 'the Proud', 'Prow-Gleam', 'the Red', 'the Serpent-Tongue', 'the Shape-Shifting', 'the Showy', 'Silk-Beard', 'Skinflint', 'the Skull-Splitter', 'Sleekstone-Eye', 'Small-Nose', 'Smooth-Tongue', 'the Southerner', 'the Stout', 'the Stubborn', 'Thin-Hair', 'the Tidbit', 'the Unruly', 'the Wealthy', 'Wartooth', 'the Whelp', 'the White', 'Wisdom-Slope', 'the Witch-Breaker', 'the Word-Master', 'the Wry-Mouth', 'the Wry-Neck', 'the Yeoman'],
}
